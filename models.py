from flask_sqlalchemy import SQLAlchemy
from app import app
db = SQLAlchemy(app)


class Balance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    balance = db.Column(db.Float,  unique=False, nullable=False)
    comment = db.Column(db.String(120), unique=False, nullable=True)


class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120),  unique=True, nullable=False)
    price = db.Column(db.Float,  unique=False, nullable=False)
    quantity = db.Column(db.Float,  unique=False, nullable=False)


class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime,  unique=False, nullable=False)
    comment = db.Column(db.String(120), unique=False, nullable=True)
