from flask import Flask, render_template, url_for, request
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///my.db'
db = SQLAlchemy(app)


class Balance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    balance = db.Column(db.Float,  unique=False, nullable=False)
    comment = db.Column(db.String(120), unique=False, nullable=True)


class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120),  unique=True, nullable=False)
    price = db.Column(db.Float,  unique=False, nullable=False)
    quantity = db.Column(db.Float,  unique=False, nullable=False)


class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime,  unique=False, nullable=False)
    comment = db.Column(db.String(120), unique=False, nullable=True)


@app.route("/")
def index():
    balances = Balance.query.all()
    balance = balances[-1].balance
    products = Products.query.all()
    return render_template("index.html", balances=balances,
                                        balance=balance,
                                        products=products)

@app.route("/historia/")
@app.route("/historia/<int:line_from>/<int:line_to>")
def historia(line_from=None, line_to=None):
    if line_from and line_to:
        print("Ograniczenie")
        history = History.query[line_from:line_to+1]
    else:
        history = History.query.all()
    return render_template("historia.html", history=history)

@app.route("/change_balance/", methods=['GET', 'POST'])
def change_balance():
    new_balance = float(request.form.get('saldo'))
    comment = request.form.get('komentarz')
    if new_balance and comment:
        add_balance(new_balance, comment)        
    balances = Balance.query.all()
    balance = balances[-1].balance
    products = Products.query.all()
    return render_template("index.html", balances=balances,
                                        balance=balance,
                                        products=products)

@app.route("/add_product/", methods=['GET', 'POST'])
def add_product():
    name = request.form.get('zakup_produkt')
    price = float(request.form.get('zakup_cena'))
    new_quantity = float(request.form.get('zakup_ilosc'))
    new_balance = -price * new_quantity
    product_in_db = Products.query.filter_by(name=name).first()
    if name and price and new_quantity:
        if product_in_db:
            product_in_db.name = product_in_db.name
            product_in_db.price = price
            product_in_db.quantity = product_in_db.quantity + new_quantity
            db.session.commit()
            to_history = f'ZAKUP - Zaktualizowano produkt {name}, cena={price}, ilość={new_quantity}'
        else:
            product = Products(
                name=name,
                price=price,
                quantity=new_quantity
            )
            db.session.add(product)
            db.session.commit()
            to_history = f'ZAKUP - Dodano produkt {name}, cena={price}, ilość={new_quantity}'
        add_to_history(to_history)
        add_balance(new_balance, to_history)
    return render_template("index.html")

@app.route("/sell_product/", methods=['GET', 'POST'])
def sell_product():
    name = request.form.get('sprzedaz_produkt')
    price = float(request.form.get('sprzedaz_cena'))
    new_quantity = float(request.form.get('sprzedaz_ilosc'))
    new_balance = price * new_quantity
    product_in_db = Products.query.filter_by(name=name).first()
    if name and price and new_quantity:
        if product_in_db and (product_in_db.quantity >= new_quantity):
            product_in_db.name = product_in_db.name
            product_in_db.price = price
            product_in_db.quantity = product_in_db.quantity - new_quantity
            db.session.commit()
            to_history = f'SPRZEDAŻ - Zaktualizowano produkt {name}, cena={price}, ilość= -{new_quantity}'
            add_balance(new_balance, to_history)
        else:
            print("Brak produktu w bazie")
            to_history = f'SPRZEDAŻ - Brak produktu w bazie'
        add_to_history(to_history)
        return render_template("index.html")

def add_to_history(message):
    date = datetime.now()
    to_history = History(
        date=date,
        comment=message
    )
    db.session.add(to_history)
    db.session.commit()

def add_balance(new_balance, comment):
    balances = Balance.query.all()
    balance = balances[-1].balance
    comment=f'SALDO - {comment}, Zmiana: {new_balance}'
    add_balance = Balance(
        balance=balance + new_balance,
        comment=comment
    )
    db.session.add(add_balance)
    db.session.commit()
    add_to_history(comment)